import "../App.css";
import Language from "../components/LanguageComponents";
import Header from "../components/Header";
import { useParams } from "react-router-dom";

function JavaScript() {
  let { id } = useParams();
  const languageList = [
    {
      name: "HTML & CSS",
      image:
        "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg",
    },
    {
      name: "JavaScript",
      image:
        "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg",
    },
    {
      name: "React",
      image:
        "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg",
    },
    {
      name: "Ruby",
      image:
        "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg",
    },
    {
      name: "Ruby on Rails",
      image:
        "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg",
    },
    {
      name: "Python",
      image:
        "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg",
    },
  ];

  return (
    <div className="App">
      <Header />
      <h1>ID : {id}</h1>
      {languageList.map((languageItem) => {
        if (languageItem.name == id) {
          return (
            <Language name={languageItem.name} image={languageItem.image} />
          );
        }
      })}
    </div>
  );
}

export default JavaScript;
